{-# LANGUAGE DeriveGeneric, OverloadedStrings #-}
import           Control.Monad
import           Control.Monad.IO.Class
import           Control.Monad.Random
import           Data.Aeson
import qualified Data.ByteString.Lazy.Char8 as BS
import           Data.Conduit
import           Data.Text
import           Data.Word
import           GHC.Generics
import qualified Network.HTTP.Conduit as C
import           Numeric
import           System.Environment

data EvalRequest = EvalRequest { id :: Maybe Text
                               , program :: Maybe Text
                               , arguments :: [Text] 
                               } deriving (Show, Generic)

data EvalResponse = EvalResponse { status :: Text
                                 , outputs :: Maybe [Text]
                                 , message :: Maybe Text
                                 } deriving (Show, Generic)

instance FromJSON EvalRequest
instance ToJSON EvalRequest
instance FromJSON EvalResponse
instance ToJSON EvalResponse

data Request = Request { requestPath :: String
                       , requestBody :: BS.ByteString
                       }

data Response = Response { parsedRequest :: Maybe a }

instance Request EvalRequest where
    requestPath = const "eval"

instance Response EvalResponse

randWord64s     :: RandomGen g => Int -> Rand g [Word64]
randWord64s num = replicateM num getRandom

generateInput :: RandomGen g => Int -> g -> [Text]
generateInput n gen = Prelude.map (pack . ("0x"++) . (\x -> showHex x "")) $ fst $ runRand (randWord64s n) gen

webApiUrl = "http://icfpc2013.cloudapp.net/"

makeRequest :: Request -> IO Response
makeRequest (Request path body) = runResourceT $ do
  manager <- liftIO $ C.newManager C.def
  req' <- liftIO . C.parseUrl $ webApiUrl ++ path ++ "?auth=0187LBKE08MXlWBfqs8JpNdG05ZAAHzB7mUgTGhAvpsH1H" -- make bearable later
  let req = req' { C.method = "POST", C.requestBody = C.RequestBodyLBS $ body }
  response <- C.httpLbs req manager
  -- return (C.responseBody response)
  return (Response $ EvalResponse  "" Nothing Nothing)

main = do
  args <- getArgs
  let n = 256
  gen <- getStdGen
  authToken <- readFile "auth-token.txt"
  let input = generateInput n gen
  case args of
    ["eval", id] -> print 1
    -- ["eval-prog", program] -> do req <- 
  BS.putStrLn $ encode $ EvalRequest (Just "1") Nothing input
