import Data.Char
import Data.Word
import qualified Data.Set as Set
import Control.DeepSeq

data BVENullaryExp = BVEConstant0 | BVEConstant1 | BVEId [Int] deriving (Eq, Show)
data BVEUnaryExp = BVENot | BVEShl1 | BVEShr1 | BVEShr4 | BVEShr16 deriving (Eq, Show)
data BVEBinaryExp = BVEAnd | BVEOr | BVEXor | BVEPlus deriving (Eq, Show)
data BVETriaryExp = BVEIf0 | BVEFold deriving (Eq, Show)

data BVExpressionTree = BVENullary BVENullaryExp
                      | BVEUnary BVEUnaryExp [BVExpressionTree]
                      | BVEBinary BVEBinaryExp [([BVExpressionTree], [BVExpressionTree])]
                      | BVETriary BVETriaryExp [([BVExpressionTree], [BVExpressionTree],
                                                 [BVExpressionTree])] deriving Show

data BVOperator = BVOIf0 | BVOFold | BVOAnd | BVOOr | BVOXor | BVOPlus
                | BVONot | BVOShl1 | BVOShr1 | BVOShr4 | BVOShr16
                deriving (Eq, Ord)
{- About memoization:
   We can memoize genExpression, and pre-build for example all trees for single-item sets and for all
   lengths though needed one. Then we can use this data to speed up generation of more complex trees.
-}

{- About code:
   If, only if I knew how to write generic functions on tuples... Looks like I need Template Haskell
   for this. I could use typeclasses for uniting functions under one name, but the code would still
   look ugly.
-}

genExpressions :: Set.Set BVOperator -> Int -> Int -> [BVExpressionTree]
genExpressions ops depth lastId = (Set.fold (\x acc -> case genExOp x of Just a -> a:acc
                                                                         Nothing -> acc)
                                   [] ops) ++ map BVENullary [BVEConstant0, BVEConstant1, BVEId [0..lastId]]
  where genExOp = genExpression ops depth lastId

genExpressions2 :: Set.Set BVOperator -> Int -> Int -> [([BVExpressionTree], [BVExpressionTree])]
genExpressions2 ops depth lastId = [ (genExps a, genExps b) | a <- [1..mDepth], b <- [1..mDepth],
                                     a + b == depth ]
  where mDepth = depth - 1
        genExps x = genExpressions ops x lastId
                                                         
genExpressions3 :: Set.Set BVOperator -> Int -> Int -> [([BVExpressionTree], [BVExpressionTree],
                                                         [BVExpressionTree])]
genExpressions3 ops depth lastId = [ (genExps a, genExps b, genExps c) | a <- [1..mDepth],
                                     b <- [1..mDepth], c <- [1..mDepth], a + b + c == depth ]
  where mDepth = depth - 2
        genExps x = genExpressions ops x lastId

genExpression :: Set.Set BVOperator -> Int -> Int -> BVOperator -> Maybe BVExpressionTree
genExpression ops depth lastId op
  | op == BVOFold && depth >= 5 = let pdepth' = pred pdepth
                                  in Just $ BVETriary BVEFold $ genExpressions3 (Set.delete BVOFold ops)
                                     pdepth' (succ lastId)
  | op == BVOIf0 && depth >= 4 = Just $ BVETriary BVEIf0 exp3
  | op == BVOAnd && depth >= 3 = Just $ BVEBinary BVEAnd exp2
  | op == BVOOr && depth >= 3 = Just $ BVEBinary BVEOr exp2
  | op == BVOXor && depth >= 3 = Just $ BVEBinary BVEXor exp2
  | op == BVOPlus && depth >= 3 = Just $ BVEBinary BVEPlus exp2
  | op == BVONot && depth >= 2 = Just $ BVEUnary BVENot exp1
  | op == BVOShl1 && depth >= 2 = Just $ BVEUnary BVEShl1 exp1
  | op == BVOShr1 && depth >= 2 = Just $ BVEUnary BVEShr1 exp1
  | op == BVOShr4 && depth >= 2 = Just $ BVEUnary BVEShr4 exp1
  | op == BVOShr16 && depth >= 2 = Just $ BVEUnary BVEShr16 exp1
  | otherwise = Nothing
  where pdepth = pred depth
        exp1 = genExpressions ops pdepth lastId
        exp2 = genExpressions2 ops pdepth lastId
        exp3 = genExpressions3 ops pdepth lastId

nameId :: Int -> String
nameId cid = case charId cid of
  Left a -> [a]
  Right a -> a : (nameNumId $ cid `div` charNum)
  where charNum = ord 'z' - ord 'a'
        charId :: Int -> Either Char Char
        charId cid
          | cid <= charNum = Left $ chr $ cid + ord 'a'
          | otherwise = Right $ either id id $ charId (cid `rem` charNum)

nameNumId :: Int -> String
nameNumId cid = case charId cid of
  Left a -> [a]
  Right a -> a : (nameNumId $ cid `div` fCharNum)
  where charNum = ord 'z' - ord 'a'
        fCharNum = charNum + 10
        charId cid
          | cid <= charNum = Left $ chr $ cid + ord 'a'
          | cid <= fCharNum = Left $ chr $ cid - charNum + ord '0'
          | otherwise = Right $ either id id $ charId (cid `rem` fCharNum)

showExpressions :: Int -> [BVExpressionTree] -> [String]
showExpressions depth exps = concatMap (showExpression depth) exps

showExpressions2 :: Int -> [([BVExpressionTree], [BVExpressionTree])] -> [(String, String)]
showExpressions2 depth exps = concatMap (\(a, b) -> [(a, b) | a <- a, b <- b])
                              $ concatMap (\(a, b) -> [(showExp a, showExp b)
                                                      | a <- a, b <- b]) exps
  where showExp = showExpression depth
                                    
showExpressions3 :: Int -> [([BVExpressionTree], [BVExpressionTree], [BVExpressionTree])] -> [(String, String, String)]
showExpressions3 depth exps = concatMap (\(a, b, c) -> [(a, b, c) | a <- a, b <- b, c <- c])
                              $ concatMap (\(a, b, c) -> [(showExp a, showExp b, showExp c)
                                                         | a <- a, b <- b, c <- c]) exps
  where showExp = showExpression depth

showOperator :: String -> String -> String
showOperator op a = "(" ++ op ++ " " ++ a ++ ")"
showOperator2 :: String -> (String, String) -> String
showOperator2 op (a, b) = "(" ++ op ++ " " ++ a ++ " " ++ b ++ ")"
showOperator3 :: String -> (String, String, String) -> String
showOperator3 op (a, b, c) = "(" ++ op ++ " " ++ a ++ " " ++ b ++ " " ++ c ++ ")"
                              
showExpression :: Int -> BVExpressionTree -> [String]
showExpression depth (BVETriary BVEFold exps) =
  map (\(a, b, c) -> "(fold " ++ a ++ " " ++ b ++ " (lambda ("
                     ++ nameId (depth + 1) ++ " " ++ nameId (depth + 2)
                     ++ ") " ++ c ++ "))"
      ) strs
  where strs = concatMap (\(a, b, c) -> [(a, b, c) | a <- a, b <- b, c <- c])
               $ concatMap (\(a, b, c) -> [(showExpression depth a, showExpression depth b, showExpression (depth + 2) c)
                                          | a <- a, b <- b, c <- c]) exps
showExpression depth (BVETriary BVEIf0 exps) = map (showOperator3 "if0") $ showExpressions3 depth exps
showExpression depth (BVEBinary op exps)
  | op == BVEAnd = showOp "and"
  | op == BVEOr = showOp "or"
  | op == BVEXor = showOp "xor"
  | op == BVEPlus = showOp "plus"
  where showOp x = map (showOperator2 x) $ showExpressions2 depth exps
showExpression depth (BVEUnary op exps)
  | op == BVENot = showOp "not"
  | op == BVEShl1 = showOp "shl1"
  | op == BVEShr1 = showOp "shr1"
  | op == BVEShr4 = showOp "shr4"
  | op == BVEShr16 = showOp "shr16"
  where showOp x = map (showOperator x) $ showExpressions depth exps
showExpression depth (BVENullary BVEConstant0) = ["0"]
showExpression depth (BVENullary BVEConstant1) = ["1"]
showExpression depth (BVENullary (BVEId ids)) = map nameId ids

showFullExpressions :: [BVExpressionTree] -> [String]
showFullExpressions exps = map (\x -> "(lambda (a) " ++ x ++ ")")
                           $ showExpressions 0 exps

main = do exprs <- showFullExpressions $ genExpressions (Set.fromList [BVONot, BVOShl1, BVOShr4, BVOPlus, BVOOr]) 10 0
          print $ map (\x -> deepseq x 1) exprs
